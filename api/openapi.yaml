openapi: 3.0.0
info:
  description: "Mobwa Payments Hub allows anyone to transfer money between different\
    \ accounts as well as manage them. It \ncurrently supports the following currencies:\
    \ United States Dollar (USD) and Singapura Dollar (SGD)\n"
  license:
    name: CC-0
  title: Mobwa Payments Hub
  version: 1.0.0
servers:
- url: /
tags:
- description: Enroll a new user
  name: Signup
- description: Everything about your users
  name: Users
- description: Access to Petstore accounts
  name: Accounts
- description: Access to Petstore transfers
  name: Transfers
paths:
  /accounts/{accountId}/transfers:
    get:
      operationId: ListTransfers
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListTransfersResponse'
          description: A list of transfers related to the the account
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: List all transfers related to the account
      tags:
      - Transfers
    post:
      operationId: CreateTransfer
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateTransferRequest'
      responses:
        "201":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CreateTransferResponse'
          description: Transfer successfully created
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Create a transfer
      tags:
      - Transfers
  /accounts/{accountId}/transfers/{transferId}:
    delete:
      operationId: DeleteTransfer
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      - description: The id of the transfers to operate on
        explode: false
        in: path
        name: transferId
        required: true
        schema:
          type: string
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeleteTransferResponse'
          description: Null response
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Delete a transfer
      tags:
      - Transfers
    get:
      operationId: GetTransfer
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      - description: The id of the transfers to operate on
        explode: false
        in: path
        name: transferId
        required: true
        schema:
          type: string
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GetTransferResponse'
          description: Expected response to a valid request
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Retrieve information for a specific transfer
      tags:
      - Transfers
    put:
      operationId: UpdateTransfer
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      - description: The id of the transfers to operate on
        explode: false
        in: path
        name: transferId
        required: true
        schema:
          type: string
        style: simple
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateTransferRequest'
      responses:
        "201":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UpdateTransferResponse'
          description: Null response
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Change transfer information
      tags:
      - Transfers
  /accounts/{accountId}/transfers/{transferId}/complete:
    post:
      operationId: CompleteTransfer
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      - description: The id of the transfer to operate on
        explode: false
        in: path
        name: transferId
        required: true
        schema:
          type: string
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CompleteTransferResponse'
          description: Expected response to a valid request
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Complete a transfer
      tags:
      - Transfers
  /accounts/{accountId}/recharge:
    post:
      operationId: RechargeAccount
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RechargeAccountRequest'
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Account'
          description: Expected response to a valid request
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Recharge the account
      tags:
      - Accounts
  /accounts/{accountId}/withdraw:
    post:
      operationId: WithdrawMoney
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/WithdrawMoneyRequest'
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Account'
          description: Expected response to a valid request
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Withdraw money from the account
      tags:
      - Accounts
  /accounts:
    get:
      operationId: ListAccount
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListAccountsResponse'
          description: A paged array of pets
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: List all accounts
      tags:
      - Accounts
    post:
      operationId: CreateAccount
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateAccountRequest'
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListTransfersResponse'
          description: A paged array of pets
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Create an account
      tags:
      - Accounts
  /users:
    get:
      operationId: ListUsers
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListUsersResponse'
          description: A paged array of pets
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: List all users
      tags:
      - Users
    post:
      operationId: CreateUser
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateUserRequest'
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ListTransfersResponse'
          description: A paged array of pets
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Create a user
      tags:
      - Users
  /accounts/{accountId}:
    get:
      operationId: GetAccount
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GetAccountResponse'
          description: Expected response to a valid request
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Get account information
      tags:
      - Accounts
    put:
      operationId: UpdateAccount
      parameters:
      - description: The id of the account
        explode: false
        in: path
        name: accountId
        required: true
        schema:
          type: string
        style: simple
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateAccountRequest'
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UpdateAccountResponse'
          description: Expected response to a valid request
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Update account information
      tags:
      - Accounts
  /users/{userId}:
    get:
      operationId: GetUser
      parameters:
      - description: The id of the user to operate on
        explode: false
        in: path
        name: userId
        required: true
        schema:
          type: string
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GetAccountResponse'
          description: Expected response to a valid request
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Get user information
      tags:
      - Users
    put:
      operationId: UpdateUser
      parameters:
      - description: The id of the user to operate on
        explode: false
        in: path
        name: userId
        required: true
        schema:
          type: string
        style: simple
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateAccountRequest'
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UpdateAccountResponse'
          description: Expected response to a valid request
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      security:
      - basicAuth: []
      summary: Update user information
      tags:
      - Users
  /signup:
    post:
      operationId: Signup
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SignUpRequest'
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SignUpResponse'
          description: Information related to the enrolled user
        default:
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: unexpected error
      summary: Signup
      tags:
      - Signup
components:
  schemas:
    WithdrawMoneyRequest:
      $ref: '#/components/schemas/RechargeAccountRequest'
    RechargeAccountRequest:
      example:
        amout: 0
      properties:
        amout:
          type: integer
      type: object
    SignUpResponse:
      example:
        role: customer
        accounts:
        - updated_at: updated_at
          balance: 0
          name: name
          created_at: created_at
          currency: USD
          id: id
        - updated_at: updated_at
          balance: 0
          name: name
          created_at: created_at
          currency: USD
          id: id
        email: email
        username: username
      properties:
        username:
          type: string
        email:
          type: string
        role:
          enum:
          - customer
          - admin
          type: string
        accounts:
          items:
            $ref: '#/components/schemas/Account'
          type: array
      type: object
    SignUpRequest:
      example:
        password: password
        email: email
        username: username
      properties:
        username:
          type: string
        email:
          type: string
        password:
          type: string
      type: object
    CreateUserRequest:
      example:
        password: password
        role: CUSTOMER
        email: email
        username: username
      properties:
        username:
          type: string
        email:
          type: string
        password:
          type: string
        role:
          enum:
          - CUSTOMER
          - ADMIN
          type: string
      type: object
    User:
      example:
        role: CUSTOMER
        email: email
        username: username
      properties:
        username:
          type: string
        email:
          type: string
        role:
          enum:
          - CUSTOMER
          - ADMIN
          type: string
      type: object
    ListUsersResponse:
      items:
        $ref: '#/components/schemas/User'
      type: array
    DeleteTransferResponse:
      $ref: '#/components/schemas/Transfer'
    CompleteTransferResponse:
      $ref: '#/components/schemas/Transfer'
    GetTransferResponse:
      $ref: '#/components/schemas/Transfer'
    CreateTransferResponse:
      $ref: '#/components/schemas/Transfer'
    UpdateTransferResponse:
      $ref: '#/components/schemas/Transfer'
    Transfer:
      example:
        auto_complete: true
        amount: 0
        updated_at: updated_at
        destination:
          account_id: account_id
        created_at: created_at
        currency: SGD
        id: id
        source:
          account_id: account_id
        message: message
        status: PENDING
      properties:
        id:
          type: string
        message:
          type: string
        amount:
          type: integer
        currency:
          enum:
          - SGD
          - USD
          type: string
        created_at:
          type: string
        updated_at:
          type: string
        status:
          enum:
          - PENDING
          - SUCCEEDED
          - FAILED
          type: string
        auto_complete:
          type: boolean
        source:
          $ref: '#/components/schemas/Transfer_source'
        destination:
          $ref: '#/components/schemas/Transfer_source'
      type: object
    CreateTransferRequest:
      example:
        auto_complete: true
        amount: 0
        destination: destination
        currency: SGD
        message: message
      properties:
        message:
          type: string
        amount:
          type: integer
        currency:
          enum:
          - SGD
          - USD
          type: string
        auto_complete:
          type: boolean
        destination:
          type: string
      type: object
    UpdateTransferRequest:
      example:
        amount: 0
        message: message
      properties:
        message:
          type: string
        amount:
          type: integer
      type: object
    ListTransfersResponse:
      items:
        $ref: '#/components/schemas/Transfer'
      type: array
    Error:
      properties:
        code:
          format: int32
          type: integer
        message:
          type: string
      required:
      - code
      - message
      type: object
    Account:
      example:
        updated_at: updated_at
        balance: 0
        name: name
        created_at: created_at
        currency: USD
        id: id
      properties:
        id:
          type: string
        name:
          type: string
        created_at:
          type: string
        currency:
          enum:
          - USD
          - SGD
          type: string
        updated_at:
          type: string
        balance:
          type: integer
      type: object
    UpdateAccountRequest:
      example:
        name: name
      properties:
        name:
          type: string
      type: object
    CreateAccountResponse:
      $ref: '#/components/schemas/Account'
    GetAccountResponse:
      $ref: '#/components/schemas/Account'
    UpdateAccountResponse:
      $ref: '#/components/schemas/Account'
    ListAccountsResponse:
      items:
        $ref: '#/components/schemas/Account'
      type: array
    CreateAccountRequest:
      example:
        name: name
      properties:
        name:
          type: string
      type: object
    Transfer_source:
      example:
        account_id: account_id
      properties:
        account_id:
          type: string
      type: object
  securitySchemes:
    basicAuth:
      scheme: basic
      type: http
