# \TransfersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CompleteTransfer**](TransfersApi.md#CompleteTransfer) | **Post** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
[**CreateTransfer**](TransfersApi.md#CreateTransfer) | **Post** /accounts/{accountId}/transfers | Create a transfer
[**DeleteTransfer**](TransfersApi.md#DeleteTransfer) | **Delete** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
[**GetTransfer**](TransfersApi.md#GetTransfer) | **Get** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
[**ListTransfers**](TransfersApi.md#ListTransfers) | **Get** /accounts/{accountId}/transfers | List all transfers related to the account
[**UpdateTransfer**](TransfersApi.md#UpdateTransfer) | **Put** /accounts/{accountId}/transfers/{transferId} | Change transfer information



## CompleteTransfer

> Transfer CompleteTransfer(ctx, accountId, transferId).Execute()

Complete a transfer

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    accountId := "accountId_example" // string | The id of the account
    transferId := "transferId_example" // string | The id of the transfer to operate on

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransfersApi.CompleteTransfer(context.Background(), accountId, transferId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransfersApi.CompleteTransfer``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CompleteTransfer`: Transfer
    fmt.Fprintf(os.Stdout, "Response from `TransfersApi.CompleteTransfer`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string** | The id of the account | 
**transferId** | **string** | The id of the transfer to operate on | 

### Other Parameters

Other parameters are passed through a pointer to a apiCompleteTransferRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateTransfer

> Transfer CreateTransfer(ctx, accountId).CreateTransferRequest(createTransferRequest).Execute()

Create a transfer

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    accountId := "accountId_example" // string | The id of the account
    createTransferRequest := *openapiclient.NewCreateTransferRequest() // CreateTransferRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransfersApi.CreateTransfer(context.Background(), accountId).CreateTransferRequest(createTransferRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransfersApi.CreateTransfer``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CreateTransfer`: Transfer
    fmt.Fprintf(os.Stdout, "Response from `TransfersApi.CreateTransfer`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string** | The id of the account | 

### Other Parameters

Other parameters are passed through a pointer to a apiCreateTransferRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **createTransferRequest** | [**CreateTransferRequest**](CreateTransferRequest.md) |  | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteTransfer

> Transfer DeleteTransfer(ctx, accountId, transferId).Execute()

Delete a transfer

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    accountId := "accountId_example" // string | The id of the account
    transferId := "transferId_example" // string | The id of the transfers to operate on

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransfersApi.DeleteTransfer(context.Background(), accountId, transferId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransfersApi.DeleteTransfer``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `DeleteTransfer`: Transfer
    fmt.Fprintf(os.Stdout, "Response from `TransfersApi.DeleteTransfer`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string** | The id of the account | 
**transferId** | **string** | The id of the transfers to operate on | 

### Other Parameters

Other parameters are passed through a pointer to a apiDeleteTransferRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTransfer

> Transfer GetTransfer(ctx, accountId, transferId).Execute()

Retrieve information for a specific transfer

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    accountId := "accountId_example" // string | The id of the account
    transferId := "transferId_example" // string | The id of the transfers to operate on

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransfersApi.GetTransfer(context.Background(), accountId, transferId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransfersApi.GetTransfer``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTransfer`: Transfer
    fmt.Fprintf(os.Stdout, "Response from `TransfersApi.GetTransfer`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string** | The id of the account | 
**transferId** | **string** | The id of the transfers to operate on | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetTransferRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ListTransfers

> []Transfer ListTransfers(ctx, accountId).Execute()

List all transfers related to the account

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    accountId := "accountId_example" // string | The id of the account

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransfersApi.ListTransfers(context.Background(), accountId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransfersApi.ListTransfers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ListTransfers`: []Transfer
    fmt.Fprintf(os.Stdout, "Response from `TransfersApi.ListTransfers`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string** | The id of the account | 

### Other Parameters

Other parameters are passed through a pointer to a apiListTransfersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**[]Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateTransfer

> Transfer UpdateTransfer(ctx, accountId, transferId).UpdateTransferRequest(updateTransferRequest).Execute()

Change transfer information

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    accountId := "accountId_example" // string | The id of the account
    transferId := "transferId_example" // string | The id of the transfers to operate on
    updateTransferRequest := *openapiclient.NewUpdateTransferRequest() // UpdateTransferRequest |  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.TransfersApi.UpdateTransfer(context.Background(), accountId, transferId).UpdateTransferRequest(updateTransferRequest).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `TransfersApi.UpdateTransfer``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `UpdateTransfer`: Transfer
    fmt.Fprintf(os.Stdout, "Response from `TransfersApi.UpdateTransfer`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**accountId** | **string** | The id of the account | 
**transferId** | **string** | The id of the transfers to operate on | 

### Other Parameters

Other parameters are passed through a pointer to a apiUpdateTransferRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **updateTransferRequest** | [**UpdateTransferRequest**](UpdateTransferRequest.md) |  | 

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

