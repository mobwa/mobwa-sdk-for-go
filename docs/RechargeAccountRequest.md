# RechargeAccountRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Amout** | Pointer to **int32** |  | [optional] 

## Methods

### NewRechargeAccountRequest

`func NewRechargeAccountRequest() *RechargeAccountRequest`

NewRechargeAccountRequest instantiates a new RechargeAccountRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRechargeAccountRequestWithDefaults

`func NewRechargeAccountRequestWithDefaults() *RechargeAccountRequest`

NewRechargeAccountRequestWithDefaults instantiates a new RechargeAccountRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAmout

`func (o *RechargeAccountRequest) GetAmout() int32`

GetAmout returns the Amout field if non-nil, zero value otherwise.

### GetAmoutOk

`func (o *RechargeAccountRequest) GetAmoutOk() (*int32, bool)`

GetAmoutOk returns a tuple with the Amout field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmout

`func (o *RechargeAccountRequest) SetAmout(v int32)`

SetAmout sets Amout field to given value.

### HasAmout

`func (o *RechargeAccountRequest) HasAmout() bool`

HasAmout returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


