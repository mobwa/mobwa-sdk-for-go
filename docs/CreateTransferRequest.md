# CreateTransferRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** |  | [optional] 
**Amount** | Pointer to **int32** |  | [optional] 
**Currency** | Pointer to **string** |  | [optional] 
**AutoComplete** | Pointer to **bool** |  | [optional] 
**Destination** | Pointer to **string** |  | [optional] 

## Methods

### NewCreateTransferRequest

`func NewCreateTransferRequest() *CreateTransferRequest`

NewCreateTransferRequest instantiates a new CreateTransferRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateTransferRequestWithDefaults

`func NewCreateTransferRequestWithDefaults() *CreateTransferRequest`

NewCreateTransferRequestWithDefaults instantiates a new CreateTransferRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *CreateTransferRequest) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *CreateTransferRequest) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *CreateTransferRequest) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *CreateTransferRequest) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetAmount

`func (o *CreateTransferRequest) GetAmount() int32`

GetAmount returns the Amount field if non-nil, zero value otherwise.

### GetAmountOk

`func (o *CreateTransferRequest) GetAmountOk() (*int32, bool)`

GetAmountOk returns a tuple with the Amount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmount

`func (o *CreateTransferRequest) SetAmount(v int32)`

SetAmount sets Amount field to given value.

### HasAmount

`func (o *CreateTransferRequest) HasAmount() bool`

HasAmount returns a boolean if a field has been set.

### GetCurrency

`func (o *CreateTransferRequest) GetCurrency() string`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *CreateTransferRequest) GetCurrencyOk() (*string, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *CreateTransferRequest) SetCurrency(v string)`

SetCurrency sets Currency field to given value.

### HasCurrency

`func (o *CreateTransferRequest) HasCurrency() bool`

HasCurrency returns a boolean if a field has been set.

### GetAutoComplete

`func (o *CreateTransferRequest) GetAutoComplete() bool`

GetAutoComplete returns the AutoComplete field if non-nil, zero value otherwise.

### GetAutoCompleteOk

`func (o *CreateTransferRequest) GetAutoCompleteOk() (*bool, bool)`

GetAutoCompleteOk returns a tuple with the AutoComplete field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAutoComplete

`func (o *CreateTransferRequest) SetAutoComplete(v bool)`

SetAutoComplete sets AutoComplete field to given value.

### HasAutoComplete

`func (o *CreateTransferRequest) HasAutoComplete() bool`

HasAutoComplete returns a boolean if a field has been set.

### GetDestination

`func (o *CreateTransferRequest) GetDestination() string`

GetDestination returns the Destination field if non-nil, zero value otherwise.

### GetDestinationOk

`func (o *CreateTransferRequest) GetDestinationOk() (*string, bool)`

GetDestinationOk returns a tuple with the Destination field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDestination

`func (o *CreateTransferRequest) SetDestination(v string)`

SetDestination sets Destination field to given value.

### HasDestination

`func (o *CreateTransferRequest) HasDestination() bool`

HasDestination returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


