# SignUpResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | Pointer to **string** |  | [optional] 
**Email** | Pointer to **string** |  | [optional] 
**Role** | Pointer to **string** |  | [optional] 
**Accounts** | Pointer to [**[]Account**](Account.md) |  | [optional] 

## Methods

### NewSignUpResponse

`func NewSignUpResponse() *SignUpResponse`

NewSignUpResponse instantiates a new SignUpResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSignUpResponseWithDefaults

`func NewSignUpResponseWithDefaults() *SignUpResponse`

NewSignUpResponseWithDefaults instantiates a new SignUpResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUsername

`func (o *SignUpResponse) GetUsername() string`

GetUsername returns the Username field if non-nil, zero value otherwise.

### GetUsernameOk

`func (o *SignUpResponse) GetUsernameOk() (*string, bool)`

GetUsernameOk returns a tuple with the Username field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUsername

`func (o *SignUpResponse) SetUsername(v string)`

SetUsername sets Username field to given value.

### HasUsername

`func (o *SignUpResponse) HasUsername() bool`

HasUsername returns a boolean if a field has been set.

### GetEmail

`func (o *SignUpResponse) GetEmail() string`

GetEmail returns the Email field if non-nil, zero value otherwise.

### GetEmailOk

`func (o *SignUpResponse) GetEmailOk() (*string, bool)`

GetEmailOk returns a tuple with the Email field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEmail

`func (o *SignUpResponse) SetEmail(v string)`

SetEmail sets Email field to given value.

### HasEmail

`func (o *SignUpResponse) HasEmail() bool`

HasEmail returns a boolean if a field has been set.

### GetRole

`func (o *SignUpResponse) GetRole() string`

GetRole returns the Role field if non-nil, zero value otherwise.

### GetRoleOk

`func (o *SignUpResponse) GetRoleOk() (*string, bool)`

GetRoleOk returns a tuple with the Role field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRole

`func (o *SignUpResponse) SetRole(v string)`

SetRole sets Role field to given value.

### HasRole

`func (o *SignUpResponse) HasRole() bool`

HasRole returns a boolean if a field has been set.

### GetAccounts

`func (o *SignUpResponse) GetAccounts() []Account`

GetAccounts returns the Accounts field if non-nil, zero value otherwise.

### GetAccountsOk

`func (o *SignUpResponse) GetAccountsOk() (*[]Account, bool)`

GetAccountsOk returns a tuple with the Accounts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAccounts

`func (o *SignUpResponse) SetAccounts(v []Account)`

SetAccounts sets Accounts field to given value.

### HasAccounts

`func (o *SignUpResponse) HasAccounts() bool`

HasAccounts returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


