# UpdateTransferRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Message** | Pointer to **string** |  | [optional] 
**Amount** | Pointer to **int32** |  | [optional] 

## Methods

### NewUpdateTransferRequest

`func NewUpdateTransferRequest() *UpdateTransferRequest`

NewUpdateTransferRequest instantiates a new UpdateTransferRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateTransferRequestWithDefaults

`func NewUpdateTransferRequestWithDefaults() *UpdateTransferRequest`

NewUpdateTransferRequestWithDefaults instantiates a new UpdateTransferRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMessage

`func (o *UpdateTransferRequest) GetMessage() string`

GetMessage returns the Message field if non-nil, zero value otherwise.

### GetMessageOk

`func (o *UpdateTransferRequest) GetMessageOk() (*string, bool)`

GetMessageOk returns a tuple with the Message field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMessage

`func (o *UpdateTransferRequest) SetMessage(v string)`

SetMessage sets Message field to given value.

### HasMessage

`func (o *UpdateTransferRequest) HasMessage() bool`

HasMessage returns a boolean if a field has been set.

### GetAmount

`func (o *UpdateTransferRequest) GetAmount() int32`

GetAmount returns the Amount field if non-nil, zero value otherwise.

### GetAmountOk

`func (o *UpdateTransferRequest) GetAmountOk() (*int32, bool)`

GetAmountOk returns a tuple with the Amount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAmount

`func (o *UpdateTransferRequest) SetAmount(v int32)`

SetAmount sets Amount field to given value.

### HasAmount

`func (o *UpdateTransferRequest) HasAmount() bool`

HasAmount returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


