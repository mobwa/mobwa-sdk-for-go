# Go API client for mobwa

Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It 
currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD)


## Overview
This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [OpenAPI-spec](https://www.openapis.org/) from a remote server, you can easily generate an API client.

- API version: 1.0.0
- Package version: 0.20210906.0
- Build package: org.openapitools.codegen.languages.GoClientCodegen

## Installation

Install the following dependencies:

```shell
go get github.com/stretchr/testify/assert
go get golang.org/x/oauth2
go get golang.org/x/net/context
```

Put the package under your project folder and add the following in import:

```golang
import sw "./mobwa"
```

To use a proxy, set the environment variable `HTTP_PROXY`:

```golang
os.Setenv("HTTP_PROXY", "http://proxy_name:proxy_port")
```

## Configuration of Server URL

Default configuration comes with `Servers` field that contains server objects as defined in the OpenAPI specification.

### Select Server Configuration

For using other server than the one defined on index 0 set context value `sw.ContextServerIndex` of type `int`.

```golang
ctx := context.WithValue(context.Background(), sw.ContextServerIndex, 1)
```

### Templated Server URL

Templated server URL is formatted using default variables from configuration or from context value `sw.ContextServerVariables` of type `map[string]string`.

```golang
ctx := context.WithValue(context.Background(), sw.ContextServerVariables, map[string]string{
	"basePath": "v2",
})
```

Note, enum values are always validated and all unused variables are silently ignored.

### URLs Configuration per Operation

Each operation can use different server URL defined using `OperationServers` map in the `Configuration`.
An operation is uniquely identifield by `"{classname}Service.{nickname}"` string.
Similar rules for overriding default operation server index and variables applies by using `sw.ContextOperationServerIndices` and `sw.ContextOperationServerVariables` context maps.

```
ctx := context.WithValue(context.Background(), sw.ContextOperationServerIndices, map[string]int{
	"{classname}Service.{nickname}": 2,
})
ctx = context.WithValue(context.Background(), sw.ContextOperationServerVariables, map[string]map[string]string{
	"{classname}Service.{nickname}": {
		"port": "8443",
	},
})
```

## Documentation for API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountsApi* | [**CreateAccount**](docs/AccountsApi.md#createaccount) | **Post** /accounts | Create an account
*AccountsApi* | [**GetAccount**](docs/AccountsApi.md#getaccount) | **Get** /accounts/{accountId} | Get account information
*AccountsApi* | [**ListAccount**](docs/AccountsApi.md#listaccount) | **Get** /accounts | List all accounts
*AccountsApi* | [**RechargeAccount**](docs/AccountsApi.md#rechargeaccount) | **Post** /accounts/{accountId}/recharge | Recharge the account
*AccountsApi* | [**UpdateAccount**](docs/AccountsApi.md#updateaccount) | **Put** /accounts/{accountId} | Update account information
*AccountsApi* | [**WithdrawMoney**](docs/AccountsApi.md#withdrawmoney) | **Post** /accounts/{accountId}/withdraw | Withdraw money from the account
*SignupApi* | [**Signup**](docs/SignupApi.md#signup) | **Post** /signup | Signup
*TransfersApi* | [**CompleteTransfer**](docs/TransfersApi.md#completetransfer) | **Post** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
*TransfersApi* | [**CreateTransfer**](docs/TransfersApi.md#createtransfer) | **Post** /accounts/{accountId}/transfers | Create a transfer
*TransfersApi* | [**DeleteTransfer**](docs/TransfersApi.md#deletetransfer) | **Delete** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
*TransfersApi* | [**GetTransfer**](docs/TransfersApi.md#gettransfer) | **Get** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
*TransfersApi* | [**ListTransfers**](docs/TransfersApi.md#listtransfers) | **Get** /accounts/{accountId}/transfers | List all transfers related to the account
*TransfersApi* | [**UpdateTransfer**](docs/TransfersApi.md#updatetransfer) | **Put** /accounts/{accountId}/transfers/{transferId} | Change transfer information
*UsersApi* | [**CreateUser**](docs/UsersApi.md#createuser) | **Post** /users | Create a user
*UsersApi* | [**GetUser**](docs/UsersApi.md#getuser) | **Get** /users/{userId} | Get user information
*UsersApi* | [**ListUsers**](docs/UsersApi.md#listusers) | **Get** /users | List all users
*UsersApi* | [**UpdateUser**](docs/UsersApi.md#updateuser) | **Put** /users/{userId} | Update user information


## Documentation For Models

 - [Account](docs/Account.md)
 - [CreateAccountRequest](docs/CreateAccountRequest.md)
 - [CreateTransferRequest](docs/CreateTransferRequest.md)
 - [CreateUserRequest](docs/CreateUserRequest.md)
 - [Error](docs/Error.md)
 - [RechargeAccountRequest](docs/RechargeAccountRequest.md)
 - [SignUpRequest](docs/SignUpRequest.md)
 - [SignUpResponse](docs/SignUpResponse.md)
 - [Transfer](docs/Transfer.md)
 - [TransferSource](docs/TransferSource.md)
 - [UpdateAccountRequest](docs/UpdateAccountRequest.md)
 - [UpdateTransferRequest](docs/UpdateTransferRequest.md)
 - [User](docs/User.md)


## Documentation For Authorization



### basicAuth

- **Type**: HTTP basic authentication

Example

```golang
auth := context.WithValue(context.Background(), sw.ContextBasicAuth, sw.BasicAuth{
    UserName: "username",
    Password: "password",
})
r, err := client.Service.Operation(auth, args)
```


## Documentation for Utility Methods

Due to the fact that model structure members are all pointers, this package contains
a number of utility functions to easily obtain pointers to values of basic types.
Each of these functions takes a value of the given basic type and returns a pointer to it:

* `PtrBool`
* `PtrInt`
* `PtrInt32`
* `PtrInt64`
* `PtrFloat`
* `PtrFloat32`
* `PtrFloat64`
* `PtrString`
* `PtrTime`

## Author



