/*
Mobwa Payments Hub

Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD) 

API version: 1.0.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package mobwa

import (
	"encoding/json"
)

// TransferSource struct for TransferSource
type TransferSource struct {
	AccountId *string `json:"account_id,omitempty"`
}

// NewTransferSource instantiates a new TransferSource object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewTransferSource() *TransferSource {
	this := TransferSource{}
	return &this
}

// NewTransferSourceWithDefaults instantiates a new TransferSource object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewTransferSourceWithDefaults() *TransferSource {
	this := TransferSource{}
	return &this
}

// GetAccountId returns the AccountId field value if set, zero value otherwise.
func (o *TransferSource) GetAccountId() string {
	if o == nil || o.AccountId == nil {
		var ret string
		return ret
	}
	return *o.AccountId
}

// GetAccountIdOk returns a tuple with the AccountId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *TransferSource) GetAccountIdOk() (*string, bool) {
	if o == nil || o.AccountId == nil {
		return nil, false
	}
	return o.AccountId, true
}

// HasAccountId returns a boolean if a field has been set.
func (o *TransferSource) HasAccountId() bool {
	if o != nil && o.AccountId != nil {
		return true
	}

	return false
}

// SetAccountId gets a reference to the given string and assigns it to the AccountId field.
func (o *TransferSource) SetAccountId(v string) {
	o.AccountId = &v
}

func (o TransferSource) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.AccountId != nil {
		toSerialize["account_id"] = o.AccountId
	}
	return json.Marshal(toSerialize)
}

type NullableTransferSource struct {
	value *TransferSource
	isSet bool
}

func (v NullableTransferSource) Get() *TransferSource {
	return v.value
}

func (v *NullableTransferSource) Set(val *TransferSource) {
	v.value = val
	v.isSet = true
}

func (v NullableTransferSource) IsSet() bool {
	return v.isSet
}

func (v *NullableTransferSource) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableTransferSource(val *TransferSource) *NullableTransferSource {
	return &NullableTransferSource{value: val, isSet: true}
}

func (v NullableTransferSource) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableTransferSource) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


